# VCF Analysis Visualization - Read Me #

Visualize information about data extracted from a Variant Call Format (VCF) file.

### Information Visualization - Final Project ###

* Universita' degli studi Roma Tre
* A.A. 2014 - 2015
* Group: BackInCode
* Link source: [https://bitbucket.org/backincode/infovis](https://bitbucket.org/backincode/infovis)

### Description: ###

This project visualize many informations about data extracted from a Variant Call Format (VCF) file.

* Parse and manipulate an Apache Cassandra DB dump.
* Use Leaflet library for visualize data on a map.
* Use D3.js for showing details about the analysis.
* More details about patients and variations on NCBI and Coriell websites.

### Project strucure: ###


```


	+-VCF Analysis
	|
	|------+-src
	|	   |
	|	   +--------+-css      
	|	   |	    |--------+-leaflet.css
	|      |	    |
	|	   |	    |--------+-MasterCluster.css
	|      |	    |
	|      |	    |--------+-MasterCluster.Default.css
	|      |	    |
	|	   |	    |--------+-screen.css
	|	   |
	|	   |		   		    
	|	   +--------+-data      
	|	   |		|--------+-csv
	|	   |		|		 |--------+-vcf_operations.csv
	|	   |		|
	|	   |		|--------+-bar-data.csv
	|	   |		|
	|	   |		|--------+-dataset.js
	|	   |
	|	   |				   
	|      +--------+-script
	|	   |	    |--------+-lib
	|	   |	    |		 |--------+-images
	|      |	    |		 |		  |	--------+-marker_icon.png
	|      |	    |		 |		  |	
	|      |	    |		 |		  |	--------+-marker_shadow.png
	|      |	    |		 |
	|      |	    |		 |--------+-d3.v3.min.js
	|      |	    |		 |
	|      |	    |		 |--------+-leaflet.js
	|      |	    |		 |
	|      |	    |		 |--------+-leaflet.markercluster-src.js
	|      |		|
	|      |		|	       
	|      |	    |--------+-utils
	|	   |	    |		 |--------+-csv2json.py
	|	   |	    |		 |
	|	   |	    |		 |--------+-csv2tsv.py
	|	   |	    |		 |
	|	   |	    |		 |--------+-extract_data.cql
	|	   |	    |		 |
	|	   |	    |		 |--------+-geo_tagger.sh
	|	   |	    |		 |
	|	   |	    |		 |--------+-prepare_dataset.sh
	|	   |	  	   		   		   		   
	|	   |
	|	   |--------+-index.html
	|	   |
	|	   |--------+-README.md	
	|
	|
	|--------+-launch.sh	   
```

        
### How to run: ###

For a fresh installation:

* launch the *extract_data.cql* script to create a dump from your Apache Cassandra database (not necessary);
* put your Apache Cassandra dump in CSV format (named *vcf_operations.csv*) in the src/data/csv folder (not necessary);
* open a bash shell in the src/script/utils folder and launch *prepare_dataset.sh* script.

This will automatically call some other scripts, for a proper manipulation of the input from Cassandra to form a proper input (in JSON format) for the web application. Those scripts are (in order):

* *csv2tsv.py*, needed to manipulate a temporary .tsv file;
* *geo_tagger.sh*, needed to add geographic coordinates in the dataset, instead of a generic string (e.g. GBR);
* *csv2json.py*, needed to create the proper final JSON input (*dataset.js* in src/data folder). 

If you want to use the *dataset.js* already contained in the project (or the new one just created, if you followed the previous steps), just use the *launch.sh* script, in the root folder of the project: it will open a  HTTP server with pyhton command SimpleHTTPServer on the 9000 port and open the browser on localhost:9000.