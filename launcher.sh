#!/bin/bash
#
# LAUNCHER SCRIPT FOR EXECUTION
#

# open index.html page of project and
# launch an HTTP server listening on port 9000.

if which xdg-open > /dev/null
then
  xdg-open 'http://localhost:9000/src/index.html' && python -m SimpleHTTPServer 9000
elif which gnome-open > /dev/null
then
  gnome-open 'http://localhost:9000/src/index.html' && python -m SimpleHTTPServer 9000
elif which open > /dev/null
then 
	open http://localhost:9000/src/index.html && python -m SimpleHTTPServer 9000
fi