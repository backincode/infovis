#!/usr/bin/env python

import csv
import json
import random

csvfile = open('data/csv/clean_tagged_vcf_op.csv', 'r')
jsonfile = open('data/dataset.js', 'w')

reader = csv.reader(csvfile, delimiter='\t', quotechar='|')

jsonfile.write('var addressPoints = [\n')

for row in reader:
	
	if row[11] == 'Unknow varitation':
		row[11] = -10.0
	else :
		row[11] = float(row[11])

	if row[12] == 'Unknow varitation':
		row[12] = -10.0
	else :
		row[12] = float(row[12])
	
	if row[13] == 'Unknow varitation':
		row[13] = -10.0
	else :
		row[13] = float(row[13])

	if row[14] == 'Unknow varitation':
		row[14] = -10.0
	else :
		row[14] = float(row[14])

	row[18] = float(row[18]) + random.random()
	row[19] = float(row[19]) + random.random()
	json.dump(row, jsonfile)
	jsonfile.write(',\n')

jsonfile.write('];\n')