#!/bin/bash

#change position for catch file
cd ..
cd ..

#input file
input_file='data/csv/clean_out.csv'
#output file
output_file='data/csv/tagged_vcf_operations.csv'

#tag file with position
sed -e '/GBR/s/$/	54.33	-2.26/' -e '/FIN/s/$/	64.91	26.06/' \
-e '/CHS/s/$/	29.71	108.56/' -e '/PUR/s/$/	18.19	-66.35/' \
-e '/CDX/s/$/	21.97	100.81/' -e '/CLM/s/$/	6.24	-75.58/' \
-e '/IBS/s/$/	40.20	-3.71/' -e '/PEL/s/$/	-12.05	-77.04/' \
-e '/PJL/s/$/	31.48	74.32/' -e '/KHV/s/$/	10.76	106.69/' \
-e '/ACB/s/$/	13.19	-59.53/' -e '/GWD/s/$/	13.25	-16.31/' \
-e '/ESN/s/$/	9.05	7.39/' -e '/BEB/s/$/	23.775	90.41/' \
-e '/MSL/s/$/	8.39	-11.78/' -e '/STU/s/$/	7.87	80.70/'  \
-e '/ITU/s/$/	15.87	80.77/' -e '/CEU/s/$/	39.49	-111.54/' \
-e '/YRI/s/$/	7.42	3.90/' -e '/CHB/s/$/	39.93	116.39/' \
-e '/JPT/s/$/	35.66	139.74/' -e '/LWK/s/$/	0.59	134.77/' \
-e '/ASW/s/$/	33.60	-112.12/' -e '/MXL/s/$/	34.02	-118.41/' \
-e '/TSI/s/$/	43.77	11.24/' -e '/GIH/s/$/	29.81	-95.40/' $input_file > $output_file