#!/bin/bash

#change position
cd ..
cd ..

# 1 - convert csv -> tsv
#path
csv2tsv='script/utils/csv2tsv.py'

#call script
python $csv2tsv

#paths
out_vcf_op='data/csv/out.csv'
clean_out_vcf_op='data/csv/clean_out.csv'

# 2 - script perl: convertion to ASCII text
perl -pe 's/\r\n|\n|\r/\n/g' $out_vcf_op > $clean_out_vcf_op

# 3 - geo_tagger.sh
cd script/utils

./geo_tagger.sh

cd ..
cd ..

# 4 - replace , and " with space & '-'
#paths
input_file='data/csv/tagged_vcf_operations.csv'
output_file='data/csv/clean_tagged_vcf_op.csv'

sed -e 's/\"/ /g' -e 's/\,/ \- /g' $input_file > $output_file

# 5 - convert tsv -> array json
#path
csv2json='script/utils/csv2json.py'

#call script
python $csv2json

#removing temporary files
rm data/csv/out.csv data/csv/clean_out.csv data/csv/tagged_vcf_operations.csv data/csv/clean_tagged_vcf_op.csv
