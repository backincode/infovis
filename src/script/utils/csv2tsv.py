#!/usr/env/python

import csv

with open("data/csv/vcf_operations.csv", mode="rU") as infile:
    reader = csv.reader(infile, delimiter=',')    
    with open("data/csv/out.csv", mode="w") as outfile:
        writer = csv.writer(outfile, delimiter='\t')
        for row in reader:
        	writer.writerow(row)